===== Summary of Tests for pass_year(+) =====
2/2 tests passed.
2/2 tests passed with same number of solutions.
0/2 tests passed with same resolution tree.


Generated Arguments for + in pass_year(+)
[[john, mary]]

===== Test Queries for pass_year(+) =====
model resolution tree for query: pass_year(john)
model call 1: [[pass_exams(john),pass_cwks(john),pass_projs(john)], [true, pass_cwks(john),pass_projs(john)], [true, pass_projs(john)], [true]]
student resolution tree for query: pass_year(john)
student call 1: [[pass_cwks(john),pass_projs(john),pass_exams(john)], [true, pass_projs(john),pass_exams(john)], [true, pass_exams(john)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[pass_cwks(john),pass_projs(john),pass_exams(john)] is ordered differently.
[true, pass_projs(john),pass_exams(john)] is not found in the model resolution tree.
[true, pass_exams(john)] is not found in the model resolution tree.
PASSED unit test.

model resolution tree for query: pass_year(mary)
model call 1: [[pass_exams(mary),pass_cwks(mary),pass_projs(mary)], [false, pass_cwks(mary),pass_projs(mary)], [false]]
student resolution tree for query: pass_year(mary)
student call 1: [[pass_cwks(mary),pass_projs(mary),pass_exams(mary)], [true, pass_projs(mary),pass_exams(mary)], [false, pass_exams(mary)], [false]]
Remarks:
Depth of submission's resolution tree is deeper than model solution's by 1. 
Consider optimising with built-in predicates or by reducing recursion.
[pass_cwks(mary),pass_projs(mary),pass_exams(mary)] is ordered differently.
[true, pass_projs(mary),pass_exams(mary)] is not found in the model resolution tree.
[false, pass_exams(mary)] is not found in the model resolution tree.
[false] occurred too late.
PASSED unit test.

===== Summary of Tests for pass_year(+) =====
2/2 tests passed.
2/2 tests passed with same number of solutions.
0/2 tests passed with same resolution tree.


