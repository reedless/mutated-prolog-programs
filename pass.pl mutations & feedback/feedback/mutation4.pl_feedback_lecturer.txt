===== Summary of Tests for pass_year(+) =====
2/2 tests passed.
2/2 tests passed with same number of solutions.
0/2 tests passed with same resolution tree.


Generated Arguments for + in pass_year(+)
[[john, mary]]

===== Test Queries for pass_year(+) =====
model resolution tree for query: pass_year(john)
model call 1: [[pass_exams(john),pass_cwks(john),pass_projs(john)], [true, pass_cwks(john),pass_projs(john)], [true, pass_projs(john)], [true]]
student resolution tree for query: pass_year(john)
student call 1: [[pass_projs(john),pass_exams(john),pass_cwks(john)], [true, pass_exams(john),pass_cwks(john)], [true, pass_cwks(john)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[pass_projs(john),pass_exams(john),pass_cwks(john)] is ordered differently.
[true, pass_exams(john),pass_cwks(john)] is not found in the model resolution tree.
[true, pass_cwks(john)] is not found in the model resolution tree.
PASSED unit test.

model resolution tree for query: pass_year(mary)
model call 1: [[pass_exams(mary),pass_cwks(mary),pass_projs(mary)], [false, pass_cwks(mary),pass_projs(mary)], [false]]
student resolution tree for query: pass_year(mary)
student call 1: [[pass_projs(mary),pass_exams(mary),pass_cwks(mary)], [false, pass_exams(mary),pass_cwks(mary)], [false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[pass_projs(mary),pass_exams(mary),pass_cwks(mary)] is ordered differently.
[false, pass_exams(mary),pass_cwks(mary)] is not found in the model resolution tree.
PASSED unit test.

===== Summary of Tests for pass_year(+) =====
2/2 tests passed.
2/2 tests passed with same number of solutions.
0/2 tests passed with same resolution tree.


