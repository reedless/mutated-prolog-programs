===== Summary of Tests for a. =====
1/1 tests passed.
1/1 tests passed with same number of solutions.
0/1 tests passed with same resolution tree.


===== Summary of Tests for b. =====
0/1 tests passed.
0/1 tests passed with same number of solutions.
0/1 tests passed with same resolution tree.


===== Test Queries for a. =====
Resolution tree for query: a
call 1: [[f], [true]]
call 2: [[b,c], [g, c], [false, c], [false]]
call 3: [[b,c], [true, c], [true]]
Remarks:
Depth of submission's resolution tree is shallower than model solution's by 1. 
Predicates may be over-specified, consider making predicates more general.
[f] is not found in the model resolution tree.
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!
Depth of submission's resolution tree is deeper than model solution's by 1. 
Consider optimising with built-in predicates or by reducing recursion.
[b,c] is not found in the model resolution tree.
[true, c] is not found in the model resolution tree.
PASSED unit test.

===== Summary of Tests for a. =====
1/1 tests passed.
1/1 tests passed with same number of solutions.
0/1 tests passed with same resolution tree.


===== Test Queries for b. =====
Resolution tree for query: b
call 1: [[g], [false]]
call 2: [[true]]
Remarks:
Depth of submission's resolution tree is deeper than model solution's by 1. 
Consider optimising with built-in predicates or by reducing recursion.
Depth of submission's resolution tree is shallower than model solution's by 1. 
Predicates may be over-specified, consider making predicates more general.
FAILED unit test.

===== Summary of Tests for b. =====
0/1 tests passed.
0/1 tests passed with same number of solutions.
0/1 tests passed with same resolution tree.


