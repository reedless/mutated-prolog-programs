===== Summary of Tests for a. =====
1/1 tests passed.
1/1 tests passed with same number of solutions.
1/1 tests passed with same resolution tree.


===== Summary of Tests for b. =====
1/1 tests passed.
1/1 tests passed with same number of solutions.
1/1 tests passed with same resolution tree.


===== Test Queries for a. =====
model resolution tree for query: a
model call 1: [[b,c], [true, c], [true]]
model call 2: [[b,c], [g, c], [false, c], [false]]
model call 3: [[f], [true]]
student resolution tree for query: a
student call 1: [[b,c], [true, c], [true]]
student call 2: [[b,c], [g, c], [false, c], [false]]
student call 3: [[f], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
PASSED unit test.

===== Summary of Tests for a. =====
1/1 tests passed.
1/1 tests passed with same number of solutions.
1/1 tests passed with same resolution tree.


===== Test Queries for b. =====
model resolution tree for query: b
model call 1: [[true]]
model call 2: [[g], [false]]
student resolution tree for query: b
student call 1: [[true]]
student call 2: [[g], [false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
PASSED unit test.

===== Summary of Tests for b. =====
1/1 tests passed.
1/1 tests passed with same number of solutions.
1/1 tests passed with same resolution tree.


