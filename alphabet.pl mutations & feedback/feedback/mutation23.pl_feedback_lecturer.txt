===== Summary of Tests for a. =====
1/1 tests passed.
1/1 tests passed with same number of solutions.
0/1 tests passed with same resolution tree.


===== Summary of Tests for b. =====
0/1 tests passed.
0/1 tests passed with same number of solutions.
0/1 tests passed with same resolution tree.


===== Test Queries for a. =====
model resolution tree for query: a
model call 1: [[b,c], [true, c], [true]]
model call 2: [[b,c], [g, c], [false, c], [false]]
model call 3: [[f], [true]]
student resolution tree for query: a
student call 1: [[f], [true]]
student call 2: [[b,c], [g, c], [false, c], [false]]
student call 3: [[b,c], [true, c], [true]]
Remarks:
Depth of submission's resolution tree is shallower than model solution's by 1. 
Predicates may be over-specified, consider making predicates more general.
[f] is not found in the model resolution tree.
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
Depth of submission's resolution tree is deeper than model solution's by 1. 
Consider optimising with built-in predicates or by reducing recursion.
[b,c] is not found in the model resolution tree.
[true, c] is not found in the model resolution tree.
PASSED unit test.

===== Summary of Tests for a. =====
1/1 tests passed.
1/1 tests passed with same number of solutions.
0/1 tests passed with same resolution tree.


===== Test Queries for b. =====
model resolution tree for query: b
model call 1: [[true]]
model call 2: [[g], [false]]
student resolution tree for query: b
student call 1: [[g], [false]]
student call 2: [[true]]
Remarks:
Depth of submission's resolution tree is deeper than model solution's by 1. 
Consider optimising with built-in predicates or by reducing recursion.
Depth of submission's resolution tree is shallower than model solution's by 1. 
Predicates may be over-specified, consider making predicates more general.
FAILED unit test.

===== Summary of Tests for b. =====
0/1 tests passed.
0/1 tests passed with same number of solutions.
0/1 tests passed with same resolution tree.


