===== Summary of Tests for happy(+) =====
3/3 tests passed.
3/3 tests passed with same number of solutions.
3/3 tests passed with same resolution tree.


===== Summary of Tests for happyalso(+) =====
3/3 tests passed.
3/3 tests passed with same number of solutions.
0/3 tests passed with same resolution tree.


===== Summary of Tests for happytoo(+) =====
3/3 tests passed.
3/3 tests passed with same number of solutions.
0/3 tests passed with same resolution tree.


Generated Arguments for + in happy(+)
[[bob, frank, chris]]

===== Test Queries for happy(+) =====
model resolution tree for query: happy(bob)
model call 1: [[person(bob),likes(chris,bob)], [true, likes(chris,bob)], [true]]
student resolution tree for query: happy(bob)
student call 1: [[person(bob),likes(chris,bob)], [true, likes(chris,bob)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.

model resolution tree for query: happy(frank)
model call 1: [[person(frank),likes(bob,frank)], [true, likes(bob,frank)], [true]]
model call 2: [[person(frank),likes(chris,frank)], [true, likes(chris,frank)], [true]]
student resolution tree for query: happy(frank)
student call 1: [[person(frank),likes(bob,frank)], [true, likes(bob,frank)], [true]]
student call 2: [[person(frank),likes(chris,frank)], [true, likes(chris,frank)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.

model resolution tree for query: happy(chris)
model call 1: [[person(chris),likes(_423,chris)], [true, likes(_423,chris)], [false]]
student resolution tree for query: happy(chris)
student call 1: [[person(chris),likes(_426,chris)], [true, likes(_426,chris)], [false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree after unifying arbitrary variables.

===== Summary of Tests for happy(+) =====
3/3 tests passed.
3/3 tests passed with same number of solutions.
3/3 tests passed with same resolution tree.


Generated Arguments for + in happyalso(+)
[[bob, frank, chris]]

===== Test Queries for happyalso(+) =====
model resolution tree for query: happyalso(bob)
model call 1: [[person(bob),;(->(likes(chris,bob),true),fail)], [true, ;(->(likes(chris,bob),true),fail)], [true]]
student resolution tree for query: happyalso(bob)
student call 1: [[person(bob),\+(\+(likes(_508,bob)))], [true, \+(\+(likes(_508,bob)))], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[person(bob),\+(\+(likes(_508,bob)))] is not found in the model resolution tree.
[true, \+(\+(likes(_508,bob)))] is not found in the model resolution tree.

model resolution tree for query: happyalso(frank)
model call 1: [[person(frank),;(->(likes(bob,frank),true),fail)], [true, ;(->(likes(bob,frank),true),fail)], [true]]
student resolution tree for query: happyalso(frank)
student call 1: [[person(frank),\+(\+(likes(_514,frank)))], [true, \+(\+(likes(_514,frank)))], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[person(frank),\+(\+(likes(_514,frank)))] is not found in the model resolution tree.
[true, \+(\+(likes(_514,frank)))] is not found in the model resolution tree.

model resolution tree for query: happyalso(chris)
model call 1: [[person(chris),;(->(likes(_515,chris),true),fail)], [true, ;(->(likes(_515,chris),true),fail)], [false]]
student resolution tree for query: happyalso(chris)
student call 1: [[person(chris),\+(\+(likes(_520,chris)))], [true, \+(\+(likes(_520,chris)))], [false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[person(chris),\+(\+(likes(_520,chris)))] is not found in the model resolution tree.
[true, \+(\+(likes(_520,chris)))] is not found in the model resolution tree.

===== Summary of Tests for happyalso(+) =====
3/3 tests passed.
3/3 tests passed with same number of solutions.
0/3 tests passed with same resolution tree.


Generated Arguments for + in happytoo(+)
[[bob, frank, chris]]

===== Test Queries for happytoo(+) =====
model resolution tree for query: happytoo(bob)
model call 1: [[person(bob),\+(\+(likes(_519,bob)))], [true, \+(\+(likes(_519,bob)))], [true]]
student resolution tree for query: happytoo(bob)
student call 1: [[person(bob),;(->(likes(chris,bob),true),fail)], [true, ;(->(likes(chris,bob),true),fail)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[person(bob),;(->(likes(chris,bob),true),fail)] is not found in the model resolution tree.
[true, ;(->(likes(chris,bob),true),fail)] is not found in the model resolution tree.

model resolution tree for query: happytoo(frank)
model call 1: [[person(frank),\+(\+(likes(_529,frank)))], [true, \+(\+(likes(_529,frank)))], [true]]
student resolution tree for query: happytoo(frank)
student call 1: [[person(frank),;(->(likes(bob,frank),true),fail)], [true, ;(->(likes(bob,frank),true),fail)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[person(frank),;(->(likes(bob,frank),true),fail)] is not found in the model resolution tree.
[true, ;(->(likes(bob,frank),true),fail)] is not found in the model resolution tree.

model resolution tree for query: happytoo(chris)
model call 1: [[person(chris),\+(\+(likes(_535,chris)))], [true, \+(\+(likes(_535,chris)))], [false]]
student resolution tree for query: happytoo(chris)
student call 1: [[person(chris),;(->(likes(_536,chris),true),fail)], [true, ;(->(likes(_536,chris),true),fail)], [false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[person(chris),;(->(likes(_536,chris),true),fail)] is not found in the model resolution tree.
[true, ;(->(likes(_536,chris),true),fail)] is not found in the model resolution tree.

===== Summary of Tests for happytoo(+) =====
3/3 tests passed.
3/3 tests passed with same number of solutions.
0/3 tests passed with same resolution tree.


