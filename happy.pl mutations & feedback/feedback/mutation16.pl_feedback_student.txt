===== Summary of Tests for happy(+) =====
3/3 tests passed.
3/3 tests passed with same number of solutions.
3/3 tests passed with same resolution tree.


===== Summary of Tests for happyalso(+) =====
3/3 tests passed.
3/3 tests passed with same number of solutions.
3/3 tests passed with same resolution tree.


===== Summary of Tests for happytoo(+) =====
3/3 tests passed.
3/3 tests passed with same number of solutions.
3/3 tests passed with same resolution tree.


===== Test Queries for happy(+) =====
Resolution tree for query: happy(bob)
call 1: [[person(bob),likes(chris,bob)], [true, likes(chris,bob)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!

Resolution tree for query: happy(frank)
call 1: [[person(frank),likes(bob,frank)], [true, likes(bob,frank)], [true]]
call 2: [[person(frank),likes(chris,frank)], [true, likes(chris,frank)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!

Resolution tree for query: happy(chris)
call 1: [[person(chris),likes(_426,chris)], [true, likes(_426,chris)], [false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree after unifying arbitrary variables. Well done!

===== Summary of Tests for happy(+) =====
3/3 tests passed.
3/3 tests passed with same number of solutions.
3/3 tests passed with same resolution tree.


===== Test Queries for happyalso(+) =====
Resolution tree for query: happyalso(bob)
call 1: [[person(bob),;(->(likes(chris,bob),true),fail)], [true, ;(->(likes(chris,bob),true),fail)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!

Resolution tree for query: happyalso(frank)
call 1: [[person(frank),;(->(likes(bob,frank),true),fail)], [true, ;(->(likes(bob,frank),true),fail)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!

Resolution tree for query: happyalso(chris)
call 1: [[person(chris),;(->(likes(_522,chris),true),fail)], [true, ;(->(likes(_522,chris),true),fail)], [false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree after unifying arbitrary variables. Well done!

===== Summary of Tests for happyalso(+) =====
3/3 tests passed.
3/3 tests passed with same number of solutions.
3/3 tests passed with same resolution tree.


===== Test Queries for happytoo(+) =====
Resolution tree for query: happytoo(bob)
call 1: [[person(bob),\+(\+(likes(_522,bob)))], [true, \+(\+(likes(_522,bob)))], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree after unifying arbitrary variables. Well done!

Resolution tree for query: happytoo(frank)
call 1: [[person(frank),\+(\+(likes(_528,frank)))], [true, \+(\+(likes(_528,frank)))], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree after unifying arbitrary variables. Well done!

Resolution tree for query: happytoo(chris)
call 1: [[person(chris),\+(\+(likes(_534,chris)))], [true, \+(\+(likes(_534,chris)))], [false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree after unifying arbitrary variables. Well done!

===== Summary of Tests for happytoo(+) =====
3/3 tests passed.
3/3 tests passed with same number of solutions.
3/3 tests passed with same resolution tree.


