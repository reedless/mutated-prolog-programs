===== Summary of Tests for happy(+) =====
3/3 tests passed.
2/3 tests passed with same number of solutions.
0/3 tests passed with same resolution tree.


===== Summary of Tests for happyalso(+) =====
3/3 tests passed.
2/3 tests passed with same number of solutions.
0/3 tests passed with same resolution tree.


===== Summary of Tests for happytoo(+) =====
3/3 tests passed.
3/3 tests passed with same number of solutions.
0/3 tests passed with same resolution tree.


===== Test Queries for happy(+) =====
Resolution tree for query: happy(bob)
call 1: [[person(bob),\+(\+(likes(_418,bob)))], [true, \+(\+(likes(_418,bob)))], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[person(bob),\+(\+(likes(_418,bob)))] is not found in the model resolution tree.
[true, \+(\+(likes(_418,bob)))] is not found in the model resolution tree.

Resolution tree for query: happy(frank)
call 1: [[person(frank),\+(\+(likes(_492,frank)))], [true, \+(\+(likes(_492,frank)))], [true]]
Remarks:
Too few answers. Expected 2 answer(s), got 1 answer(s) instead. 
Consider removing cuts or adding additional predicates.
Depth of submission's resolution tree is exactly the same as model solution's.
[person(frank),\+(\+(likes(_492,frank)))] is not found in the model resolution tree.
[true, \+(\+(likes(_492,frank)))] is not found in the model resolution tree.

Resolution tree for query: happy(chris)
call 1: [[person(chris),\+(\+(likes(_498,chris)))], [true, \+(\+(likes(_498,chris)))], [false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[person(chris),\+(\+(likes(_498,chris)))] is not found in the model resolution tree.
[true, \+(\+(likes(_498,chris)))] is not found in the model resolution tree.

===== Summary of Tests for happy(+) =====
3/3 tests passed.
2/3 tests passed with same number of solutions.
0/3 tests passed with same resolution tree.


===== Test Queries for happyalso(+) =====
Resolution tree for query: happyalso(bob)
call 1: [[person(bob),likes(chris,bob)], [true, likes(chris,bob)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[person(bob),likes(chris,bob)] is not found in the model resolution tree.
[true, likes(chris,bob)] is not found in the model resolution tree.

Resolution tree for query: happyalso(frank)
call 1: [[person(frank),likes(bob,frank)], [true, likes(bob,frank)], [true]]
call 2: [[person(frank),likes(chris,frank)], [true, likes(chris,frank)], [true]]
Remarks:
Too many answers. Expected 1 answer(s), got 2 answer(s) instead. 

Depth of submission's resolution tree is exactly the same as model solution's.
[person(frank),likes(bob,frank)] is not found in the model resolution tree.
[true, likes(bob,frank)] is not found in the model resolution tree.

Resolution tree for query: happyalso(chris)
call 1: [[person(chris),likes(_516,chris)], [true, likes(_516,chris)], [false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[person(chris),likes(_516,chris)] is not found in the model resolution tree.
[true, likes(_516,chris)] is not found in the model resolution tree.

===== Summary of Tests for happyalso(+) =====
3/3 tests passed.
2/3 tests passed with same number of solutions.
0/3 tests passed with same resolution tree.


===== Test Queries for happytoo(+) =====
Resolution tree for query: happytoo(bob)
call 1: [[person(bob),;(->(likes(chris,bob),true),fail)], [true, ;(->(likes(chris,bob),true),fail)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[person(bob),;(->(likes(chris,bob),true),fail)] is not found in the model resolution tree.
[true, ;(->(likes(chris,bob),true),fail)] is not found in the model resolution tree.

Resolution tree for query: happytoo(frank)
call 1: [[person(frank),;(->(likes(bob,frank),true),fail)], [true, ;(->(likes(bob,frank),true),fail)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[person(frank),;(->(likes(bob,frank),true),fail)] is not found in the model resolution tree.
[true, ;(->(likes(bob,frank),true),fail)] is not found in the model resolution tree.

Resolution tree for query: happytoo(chris)
call 1: [[person(chris),;(->(likes(_536,chris),true),fail)], [true, ;(->(likes(_536,chris),true),fail)], [false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
[person(chris),;(->(likes(_536,chris),true),fail)] is not found in the model resolution tree.
[true, ;(->(likes(_536,chris),true),fail)] is not found in the model resolution tree.

===== Summary of Tests for happytoo(+) =====
3/3 tests passed.
3/3 tests passed with same number of solutions.
0/3 tests passed with same resolution tree.


