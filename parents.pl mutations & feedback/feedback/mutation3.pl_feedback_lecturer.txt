===== Summary of Tests for person(+) =====
2/3 tests passed.
2/3 tests passed with same number of solutions.
2/3 tests passed with same resolution tree.


===== Summary of Tests for number_of_parents(+,+) =====
8/9 tests passed.
8/9 tests passed with same number of solutions.
8/9 tests passed with same resolution tree.


Generated Arguments for + in person(+)
[[adam, eve, peter]]

===== Test Queries for person(+) =====
model resolution tree for query: person(adam)
model call 1: [[true]]
student resolution tree for query: person(adam)
student call 1: [[true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
PASSED unit test.

model resolution tree for query: person(eve)
model call 1: [[true]]
student resolution tree for query: person(eve)
student call 1: [[true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
PASSED unit test.

model resolution tree for query: person(peter)
model call 1: [[true]]
student resolution tree for query: person(peter)
student call 1: [[false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
FAILED unit test.

===== Summary of Tests for person(+) =====
2/3 tests passed.
2/3 tests passed with same number of solutions.
2/3 tests passed with same resolution tree.


Generated Arguments for + in number_of_parents(+,+)
[[adam, eve, peter], [0, 1, 2]]

===== Test Queries for number_of_parents(+,+) =====
model resolution tree for query: number_of_parents(adam,0)
model call 1: [[!], [true]]
student resolution tree for query: number_of_parents(adam,0)
student call 1: [[!], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
PASSED unit test.

model resolution tree for query: number_of_parents(adam,1)
model call 1: [[false]]
student resolution tree for query: number_of_parents(adam,1)
student call 1: [[false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
PASSED unit test.

model resolution tree for query: number_of_parents(adam,2)
model call 1: [[person(adam)], [true]]
student resolution tree for query: number_of_parents(adam,2)
student call 1: [[person(adam)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
PASSED unit test.

model resolution tree for query: number_of_parents(eve,0)
model call 1: [[!], [true]]
student resolution tree for query: number_of_parents(eve,0)
student call 1: [[!], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
PASSED unit test.

model resolution tree for query: number_of_parents(eve,1)
model call 1: [[false]]
student resolution tree for query: number_of_parents(eve,1)
student call 1: [[false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
PASSED unit test.

model resolution tree for query: number_of_parents(eve,2)
model call 1: [[person(eve)], [true]]
student resolution tree for query: number_of_parents(eve,2)
student call 1: [[person(eve)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
PASSED unit test.

model resolution tree for query: number_of_parents(peter,0)
model call 1: [[false]]
student resolution tree for query: number_of_parents(peter,0)
student call 1: [[false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
PASSED unit test.

model resolution tree for query: number_of_parents(peter,1)
model call 1: [[false]]
student resolution tree for query: number_of_parents(peter,1)
student call 1: [[false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree.
PASSED unit test.

model resolution tree for query: number_of_parents(peter,2)
model call 1: [[person(peter)], [true]]
student resolution tree for query: number_of_parents(peter,2)
student call 1: [[person(peter)], [false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
FAILED unit test.

===== Summary of Tests for number_of_parents(+,+) =====
8/9 tests passed.
8/9 tests passed with same number of solutions.
8/9 tests passed with same resolution tree.


