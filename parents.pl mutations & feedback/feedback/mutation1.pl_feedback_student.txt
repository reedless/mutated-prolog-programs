===== Summary of Tests for person(+) =====
2/3 tests passed.
2/3 tests passed with same number of solutions.
2/3 tests passed with same resolution tree.


===== Summary of Tests for number_of_parents(+,+) =====
8/9 tests passed.
8/9 tests passed with same number of solutions.
8/9 tests passed with same resolution tree.


===== Test Queries for person(+) =====
Resolution tree for query: person(adam)
call 1: [[false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
FAILED unit test.

Resolution tree for query: person(eve)
call 1: [[true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!
PASSED unit test.

Resolution tree for query: person(peter)
call 1: [[true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!
PASSED unit test.

===== Summary of Tests for person(+) =====
2/3 tests passed.
2/3 tests passed with same number of solutions.
2/3 tests passed with same resolution tree.


===== Test Queries for number_of_parents(+,+) =====
Resolution tree for query: number_of_parents(adam,0)
call 1: [[!], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!
PASSED unit test.

Resolution tree for query: number_of_parents(adam,1)
call 1: [[false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!
PASSED unit test.

Resolution tree for query: number_of_parents(adam,2)
call 1: [[person(adam)], [false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
FAILED unit test.

Resolution tree for query: number_of_parents(eve,0)
call 1: [[!], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!
PASSED unit test.

Resolution tree for query: number_of_parents(eve,1)
call 1: [[false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!
PASSED unit test.

Resolution tree for query: number_of_parents(eve,2)
call 1: [[person(eve)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!
PASSED unit test.

Resolution tree for query: number_of_parents(peter,0)
call 1: [[false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!
PASSED unit test.

Resolution tree for query: number_of_parents(peter,1)
call 1: [[false]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!
PASSED unit test.

Resolution tree for query: number_of_parents(peter,2)
call 1: [[person(peter)], [true]]
Remarks:
Depth of submission's resolution tree is exactly the same as model solution's.
Student resolution tree is exactly the same as the model resolution tree. Well done!
PASSED unit test.

===== Summary of Tests for number_of_parents(+,+) =====
8/9 tests passed.
8/9 tests passed with same number of solutions.
8/9 tests passed with same resolution tree.


